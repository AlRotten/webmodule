import { CrudService } from "./CrudService";
import { User } from "../Models/User";

export class PostService implements CrudService<User>{
    getAll(): User[] {
        throw new Error("Method not implemented.");
    }    
    getOne(id: number): User {
        throw new Error("Method not implemented.");
    }
    postOne(object: User): User {
        throw new Error("Method not implemented.");
    }
    updateOne(id: number, object: User): User {
        throw new Error("Method not implemented.");
    } 
    deleteOne(id: number): boolean {
        throw new Error("Method not implemented.");
    }
}