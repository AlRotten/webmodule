import { CrudService } from "./CrudService";
import { Comment } from "../Models/Comment";

export class PostService implements CrudService<Comment>{
    getAll(): Comment[] {
        throw new Error("Method not implemented.");
    }    
    getOne(id: number): Comment {
        throw new Error("Method not implemented.");
    }
    postOne(object: Comment): Comment {
        throw new Error("Method not implemented.");
    }
    updateOne(id: number, object: Comment): Comment {
        throw new Error("Method not implemented.");
    }
    deleteOne(id: number): boolean {
        throw new Error("Method not implemented.");
    }
}