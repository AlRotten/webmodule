import { CrudService } from "./CrudService";
import { Post } from "../Models/Post";

export class PostService implements CrudService<Post>{
    getAll(): Post[] {
        throw new Error("Method not implemented.");
    }    
    getOne(id: number): Post {
        throw new Error("Method not implemented.");
    }
    postOne(object: Post): Post {
        throw new Error("Method not implemented.");
    }
    updateOne(id: number, object: Post): Post {
        throw new Error("Method not implemented.");
    }
    deleteOne(id: number): boolean {
        throw new Error("Method not implemented.");
    }
}


//PATRON STRATEGY