import {User} from "./Models/User";
import {Post} from "./Models/Post";
import {Comment} from "./Models/Comment";

export const user: User = {};

export const allUsers: User [] = [];

export const post: Post = {};

export const allPosts: Post [] = [];

export const comment: Comment = {};

export const allComments: Comment[] = [];