export interface Post{
    id?: number;
    userId?: number;
    content?: string;
    timestamp?: Date;
    urlFoto?: string;
    likes?: [];
    coments?: [];
}
