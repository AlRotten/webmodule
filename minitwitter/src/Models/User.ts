export interface User{
    id?: number;
    name?: string;
    email?: string;
    password?: string;
    description?: string;
    location?: string;
    webpage?: string;
    mainpicture?: string;
}

const yolo: User = {
    name:"yolado"
}